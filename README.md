# **__libcfg__** — Simple C Config Parsing Library

Simple C libary to parse configuration files.

Building
--------

Enter the following commands to build:

```sh
	make clean
	make
```

Example Usage
-------------

Link by adding libcfg.a when compiling and include parse-conf.h for the prototypes:

```c
	struct typemap {
		char *type;
		char *val;
		struct typemap *next;
	};

	int read_conf(const char *file, struct typemap *node);
	int write_conf(const char *file, char *value);
```

Each `<type>=<val>` pair is stored in a linked-list node; one config line is stored in one
node. You must allocate space for the first struct, but following lines are allocated
automatically. You are responsible however, for free()ing ALL of the nodes yourself.

## **__Configuration__** ( ~/*.conf )

Lines are of the form `<type>=<val>`

Example:

```sh
media=mpv
browser=chromium
```
