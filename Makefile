#AR = ar
#CC = gcc
#LD = $(CC)

#LDFLAGS = -Wl,-O1,--sort-common,--as-needed,-z,relro
CFLAGS = -O2 -pipe -fstack-protector-strong -Wall -Wextra -std=c11 -pedantic-errors -D_POSIX_C_SOURCE=200809L

TARGET = libcfg.a

HDR = $(wildcard *.h)
SRC = $(wildcard *.c)
OBJ = $(patsubst %.c,%.o,$(wildcard *.c))

all: $(TARGET)

$(TARGET): $(OBJ)
#	$(AR) rcs $@ $^
	$(AR) rc $@ $^
	ranlib $(TARGET)

%.o: %.c
	$(CC) -c $(CFLAGS) $^ -o $@

libs: $(TARGET)

clean:
	rm $(TARGET) $(OBJ)
