
#ifndef PARSE_CONF_H
#define PARSE_CONF_H

struct typemap {
	char *type;
	char *val;
	struct typemap *next;
};

int read_conf(const char *, struct typemap *);
int write_conf(const char *, char *);

#endif
