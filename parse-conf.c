
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "parse-conf.h"

/*
 * struct typemap {
 *	char *type;
 *	char *val;
 *	struct typemap *next;
 * };
 *
 */

static char **str_split(char* a_str, const char a_delim)
{
	char **result = (void *)0, **ptmp = (void *)0;
	size_t count = 0;
	char *tmp = a_str;
	char *last_comma = (void *)0;
	// Include \n and \0 in strtok() call
	const char delim[2] = { a_delim, '\n' };

	/* Count how many elements will be extracted. */
	while (*tmp)
	{
		if (a_delim == *tmp)
		{
			count++;
			last_comma = tmp;
		}
		tmp++;
	}

	/* Add space for trailing token. */
	count += last_comma < (a_str + strlen(a_str) - 1);

	/* Add space for terminating null string so caller
	   knows where the list of returned strings ends. */
	count++;

	result = malloc(sizeof(char *) * count);
	ptmp = malloc(sizeof(char *) * count);

	if (result)
	{
		size_t idx  = 0;
		//char *token = strtok(a_str, delim);
		char *token = strtok_r(a_str, delim, ptmp);

		while (token)
		{
			assert(idx < count);
			*(result + idx++) = strdup(token);
			//token = strtok((void *)0, delim);
			token = strtok_r((void *)0, delim, ptmp);
		}
		assert(idx == count - 1);
		*(result + idx) = 0;
	}

	free(ptmp);
	return result;
}

int read_conf(const char *config, struct typemap *maps)
{
	FILE *fd = (void *)0;
	char **str = (void *)0, *buff = (void *)0;
	size_t n = 0;
	struct typemap *counter = maps, *temp = maps;
	counter->next = malloc(sizeof(*maps));

	if ((fd = fopen(config,"r+")) == (void *)0) {
		printf("\n fopen() error!\n");
		return 1;
	}

	while (getline(&buff, &n, fd) != -1) {
	//fprintf(stderr, "\n The bytes read from current config line are [%.*s]\n", (unsigned)strlen(buff)-1, buff);
	str = str_split(buff, '=');
	if (str) {
		counter->type = *(str);
		counter->val = *(str + 1);
	}

	counter->next = malloc(sizeof(*counter));
		temp = counter;
		counter = temp->next;
	}

	fclose(fd);
	free(str);
	free(buff);
	return 0;
}

int write_conf(const char *config, char *buff)
{
	int file = open(config, O_RDWR|O_CREAT|O_APPEND, 0666);
	if (write(file, buff, strlen(buff)) == -1) {
		printf("\n write() Error!!!\n");
		return 1;
	}

	if (close(file) == -1) {
		printf("\n close() failed\n");
		return 1;
	}

	printf("\n close() successful!\n");

	return 0;
}
